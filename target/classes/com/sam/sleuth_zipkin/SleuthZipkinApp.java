package com.sam.sleuth_zipkin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import zipkin.server.internal.EnableZipkinServer;

/**
 * @EnableZipkinServer
 * 
 * 用于开启Zipkin Server功能：分布式框架中的如果发生异常可链路追踪
 *
 */
@EnableZipkinServer
@SpringBootApplication
@EnableDiscoveryClient
public class SleuthZipkinApp 
{
    public static void main( String[] args )
    {
    	SpringApplication.run(SleuthZipkinApp.class, args);
    }
}
